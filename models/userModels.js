const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const userSchema = new mongoose.Schema({
    name:{
        type: String,
        require:[true,'Please tell us your name'],
    },
    email:{
        type: String,
        require:[true,'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail,'Please provide a valid email'],
    },
    PhoneNumber:{
        type:Number,
        require:[true,'please provide a phone number!'],
        minlength:8,
        select:false,
    },
    cid:{
        type:Number,
        require:[true,'please provide a cid number!'],
        minlength:11,
        unique: true,
    },
    password:{
        type:String,
        require:[true,'please provide a password!'],
        minlength:8,
        select:false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            // This only works on SAVE!!!
        validator: function (el) {
            return el === this.password
        },
        message: 'Passwords are not the same',
        },
    }
})

//encryption of the password
userSchema.pre('save', async function(next){
    // only run this function if password was actually modified
    if (!this.isModified('password')) return next()
    // hash the password with cost of 12
    this.password = await bcrypt.hash(this.password,12)

      // Delete passwordConfirm Field
      this.passwordConfirm = undefined
      next()

})

userSchema.pre('findOneAndUpdate', async function (next){
    const update = this.getUpdate();
    if (update.password !== '' &&
        update.password !== undefined &&
        update.password !== update.passwordConfirm){

        this.getUpdate().password = await bcrypt.hash(update.password, 12)

        update.passwordConfirm = undefined
        next()
        }else
        next()

})

userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User',userSchema)
module.exports = User