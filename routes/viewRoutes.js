const express = require('express')
const router = express.Router()
const viewsController = require('./../controllers/viewController')

router.get('/',viewsController.getLanding)
router.get('/login',viewsController.getLoginForm)
router.get('/signup',viewsController.getSignupForm)
router.get('/admin',viewsController.getAdminLogin)

module.exports = router

