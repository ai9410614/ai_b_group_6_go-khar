function validateName() {
    const namInput = document.getElementById("name");
    const nameValue = namInput.value;
    const nameError = document.querySelector(".name-error");

    if (/^[a-zA-Z]+$/.test(nameValue)){
        nameError.innerText = "";
    }else{
        nameError.innerText = "must not contain number or special Character"
    }
  }



function validateCID() {
  const cidInput = document.getElementById("cid");
  const cidValue = cidInput.value;
  const cidError = document.querySelector(".cid-error");

  
  if (cidValue.length !== 11) {
    cidError.innerText = "CID number must be exactly 11 digits";
  } else {
    cidError.innerText = "";
  }
}
function validateNumber() {
    const cidInput = document.getElementById("phone");
    const cidValue = cidInput.value;
    const cidError = document.querySelector(".phone-error");
  
    if (cidValue.length !== 8 || !/^((17)|(77))/.test(cidValue.toString())) {
      cidError.innerText = "Must be a Bhutan's sim number";
    }
    else {
      cidError.innerText = "";
    }
}
function validatePassword() {
    var password = document.getElementById("pass").value;
    var error = document.querySelector(".pass-error");
  
    if (password.length <8) {
      error.innerHTML = "Passwords should contain 8 characters";
    } else {
      error.innerHTML = "";
    }
  }
function validateRepassword() {
    var password = document.getElementById("pass").value;
    var repassword = document.getElementById("repass").value;
    var error = document.querySelector(".repass-error");
  
    if (password !== repassword) {
      error.innerHTML = "Passwords do not match";
    } else {
      error.innerHTML = "";
    }
  }