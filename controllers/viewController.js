const path = require('path')

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','signin.html'))
}

exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../','views','signup.html'))
}
exports.getAdminLogin = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','adminlogin.html'))
}

exports.getLanding = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views','landing.html'))
}
